![Cardlay](https://mms.businesswire.com/media/20180110005844/en/634403/22/cardlay_-_logo_-_gradient_thumbnail.jpg)

# Cardlay
An overly engineered Cardlay calculator who has ability to perform
calculations using a local arithmetic engine but could optionally resort
to a [web service](http://api.mathjs.org) for the evaluation. The
toggling between the local engine and web is performed by clicking on
the top-left `ToggleButton` that states online/offline.

<img alt="Basic-Advanced" src="media/basic-to-advanced-swipe.gif" width="50%">

## Architecture
![MVVM](https://cdn-images-1.medium.com/max/1600/1*8KprSpqqPtSuYObjOFPt2g.png)

The codebase honors the MVVM-pattern encouraged by Google with the use
of Android's Architecture Components. **VIEW**s observe **VIEW MODEL**s
through `LiveData`. This pattern creates a clear separation of concerns 
and allows **VIEW MODEL**s to be easily unit tested.
 
**VIEW MODEL**s communicate with so called *repositories* for obtaining
**MODEL**s from a back-end and/or a cached source. Repositories could be
shared among several **VIEW MODEL**s.

### Data Binding
The Data Binding Library allows the data sources in the app to bind to
UI components in the layouts using a declarative format. The bindings
are generated on the fly from XML layouts. Besides reducing boilerplate
code for calls such as `findViewById`, it also allows for optimized re-
layouting and/or redrawing of the UI when several attributes need to be
changed simultaneously.  

### Dependency Injection
With the use of Dagger2 and its specialized Android Dagger library the
consumer components of utility dependencies to become agnostic to where
the dependencies originate further separating the concerns between
components. The pattern also allows for seamless swapping of one
implementation with another in for instance testing environments.

In order for Dagger2 to correctly build a dependency tree, all
dependencies need to be "touched". The `AppModule` includes a
`FeaturesModule` that in turn includes the `Module`s of all features.
Each of these `Module` may now scope their dependencies.
This approach allows for future modularization of the codebase and
support for Android features such as [Instant apps](https://developer.android.com/topic/google-play-instant).


## Limitations
### Engine differences
The syntax that the web service accepts differs from the local engine.
The latter being more forgiving and lenient. An example is balancing of
parentheses:

```
// The web service considers this expression incorrect
cos(6

// while a balanced expression is accepted
cos(6)
```

An effort is made to rid the user's input from illegal syntax but only
for some basic operators as to avoiding consecutive operators **+-** but
for other operators such as factorial **!** this is not done.

### Screen size adaptation
|Keys fit|Keys squished|
|---|---|
|![Keys fit](media/screenshot_fitted_keypad.png)|![Keys squished](media/screenshot_squished_keypad.jpg)|

Due to limited time, only limited compatibility effort has been put in
the development and could clearly be noticed on some screen sizes.

## Installation
The project can be built using Gradle.

### OS X and Linux
```
./gradlew app:clean app:assembleDebug app:installDebug
```

### Windows
```
gradlew.bat app:clean app:assembleDebug app:installDebug
```

### APK binary
1. Download the [APK](apk/app-debug.apk) to an Android device
2. Run `app-debug.apk`
3. Allow the installation to take place
4. Run the app
