package se.ingenuity.cardlay.feature.calculator.util

import javax.inject.Inject

class ExpressionNormalizer @Inject
internal constructor(private val tokenizer: ExpressionTokenizer) {

    fun normalize(expression: String): String {
        val normalizedExpression = tokenizer.getNormalizedExpression(expression)

        return normalizedExpression.trim('+', '-', '/', '*')
    }
}