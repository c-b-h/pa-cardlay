package se.ingenuity.cardlay.feature.calculator.text

import android.text.SpannableStringBuilder
import android.text.TextUtils
import se.ingenuity.cardlay.feature.calculator.util.ExpressionTokenizer

/**
 * This implementation manages the replacing of invalid characters as the user types
 */
class ExpressionStringBuilder(
    text: CharSequence?, private val tokenizer: ExpressionTokenizer
) : SpannableStringBuilder(text) {

    private var edited: Boolean = false

    override fun replace(
        start: Int,
        end: Int,
        tb: CharSequence,
        tbstart: Int,
        tbend: Int
    ): SpannableStringBuilder {
        var start = start
        if (start != length || end != length) {
            edited = true
            return super.replace(start, end, tb, tbstart, tbend)
        }

        var appendExpr = tokenizer.getNormalizedExpression(tb.subSequence(tbstart, tbend).toString())
        if (appendExpr.length == 1) {
            val expr = tokenizer.getNormalizedExpression(toString())
            val firstChar = appendExpr.first()
            when (firstChar) {
                '.' -> {
                    // don't allow two decimals in the same number
                    val index = expr.lastIndexOf('.')
                    if (index != -1 && TextUtils.isDigitsOnly(expr.substring(index + 1, start))) {
                        appendExpr = ""
                    }
                }
                '+', '*', '/' -> {
                    // don't allow leading operator
                    if (start == 0) {
                        appendExpr = ""
                    } else {
                        // don't allow multiple successive operators
                        while (start > 0 && "+-*/".contains(expr[start - 1])) {
                            --start
                        }
                        // don't allow -- or +-
                        if (start > 0 && "+-".contains(expr[start - 1])) {
                            --start
                        }

                        // mark as edited since operators can always be appended
                        edited = true
                    }
                }
                '-' -> {
                    if (start > 0 && "+-".contains(expr[start - 1])) {
                        --start
                    }
                    edited = true
                }
            }
        }

        // since this is the first edit replace the entire string
        if (!edited && appendExpr.isNotEmpty()) {
            start = 0
            edited = true
        }

        appendExpr = tokenizer.getLocalizedExpression(appendExpr)
        return super.replace(start, end, appendExpr, 0, appendExpr.length)
    }
}
