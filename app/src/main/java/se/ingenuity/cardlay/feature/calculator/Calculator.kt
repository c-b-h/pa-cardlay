package se.ingenuity.cardlay.feature.calculator

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.ingenuity.cardlay.feature.calculator.api.CalculatorApi
import se.ingenuity.cardlay.feature.calculator.model.CalculationExpression
import se.ingenuity.cardlay.feature.calculator.util.ArityEvaluator
import se.ingenuity.cardlay.feature.calculator.util.ExpressionNormalizer
import se.ingenuity.cardlay.vo.Resource
import se.ingenuity.cardlay.vo.Status
import javax.inject.Inject

class Calculator @Inject constructor(
    private val context: Context,
    private val evaluator: ArityEvaluator,
    private val api: CalculatorApi,
    private val normalizer: ExpressionNormalizer
) {
    fun localEval(expression: String): LiveData<Resource<String>> {
        val result = evaluator.evaluate(expression)

        val status = if (result.second == null) {
            Status.ERROR
        } else {
            Status.SUCCESS
        }

        val message = result.third?.let { messageResId ->
            context.getString(messageResId)
        } ?: ""

        val resource = Resource(status, result.second, message)

        return object : LiveData<Resource<String>>() {
            override fun onActive() {
                if (value == null) {
                    value = resource
                }
            }
        }
    }


    fun remoteEval(expression: String): LiveData<Resource<String>> {
        val normalizedExpression = normalizer.normalize(expression)

        val evaluation = MutableLiveData<Resource<String>>()

        evaluation.value = Resource.loading(null)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = api.calcAsync(expression = CalculationExpression(listOf(normalizedExpression))).await()

                withContext(Dispatchers.Main) {
                    if (response.result == null || response.error != null) {
                        evaluation.value = Resource.error(response.error ?: "", response.result?.get(0))
                    } else {
                        evaluation.value = Resource.success(response.result[0])
                    }
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    evaluation.value = Resource.error("Unable to evaluate expression due to ${e.message}", null)
                }
            }
        }

        return evaluation
    }
}