package se.ingenuity.cardlay.feature.calculator.api

import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.POST
import se.ingenuity.cardlay.feature.calculator.model.CalculationExpression
import se.ingenuity.cardlay.feature.calculator.model.CalculationResponse

interface CalculatorApi {
    @POST("v4/")
    fun calcAsync(@Body expression: CalculationExpression): Deferred<CalculationResponse>
}