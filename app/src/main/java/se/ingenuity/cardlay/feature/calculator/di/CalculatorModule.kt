package se.ingenuity.cardlay.feature.calculator.di

import androidx.lifecycle.ViewModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import se.ingenuity.cardlay.feature.calculator.CalculatorActivity
import se.ingenuity.cardlay.feature.calculator.CalculatorViewModel
import se.ingenuity.cardlay.feature.calculator.api.CalculatorApi
import se.ingenuity.cardlay.lifecycle.ViewModelKey

@Module
abstract class CalculatorModule {
    @ContributesAndroidInjector(modules = [ScopedModule::class])
    abstract fun contributeCalculator(): CalculatorActivity

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideApi(): CalculatorApi {
            val moshi = Moshi
                .Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

            return Retrofit.Builder()
                .baseUrl("http://api.mathjs.org/")
                .client(OkHttpClient())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
                .create(CalculatorApi::class.java)
        }
    }

    @Module
    internal interface ScopedModule {
        @Binds
        @IntoMap
        @ViewModelKey(CalculatorViewModel::class)
        fun bindViewModel(viewModel: CalculatorViewModel): ViewModel
    }
}