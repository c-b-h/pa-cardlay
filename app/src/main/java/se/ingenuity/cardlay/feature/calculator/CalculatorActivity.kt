package se.ingenuity.cardlay.feature.calculator

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.PagerAdapter
import dagger.android.support.DaggerAppCompatActivity
import se.ingenuity.cardlay.R
import se.ingenuity.cardlay.databinding.ActivityCalculatorBinding
import se.ingenuity.cardlay.databinding.ViewAdvancedBinding
import se.ingenuity.cardlay.databinding.ViewBasicBinding
import se.ingenuity.cardlay.feature.calculator.text.ExpressionStringBuilder
import se.ingenuity.cardlay.feature.calculator.util.ExpressionTokenizer
import se.ingenuity.cardlay.feature.calculator.widget.CubeOutRotationTransformer
import se.ingenuity.cardlay.vo.Resource
import se.ingenuity.cardlay.vo.Status
import javax.inject.Inject

class CalculatorActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var tokenizer: ExpressionTokenizer

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityCalculatorBinding
    private lateinit var adapter: Adapter

    private lateinit var viewModel: CalculatorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_calculator)

        viewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(CalculatorViewModel::class.java)

        viewModel.response.observe(this, Observer<Resource<String>> {
            it?.let { res ->
                if (res.status == Status.LOADING) {
                    binding.progressIndicator.visibility = View.VISIBLE
                } else {
                    binding.progressIndicator.visibility = View.INVISIBLE
                }

                if (res.status == Status.SUCCESS) {
                    binding.resultField.setText(res.data)
                } else if (!res.message.isNullOrEmpty()) {
                    Toast.makeText(this, res.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        if (savedInstanceState != null) {
            val localizedExpression = tokenizer.getLocalizedExpression(
                savedInstanceState.getString(
                    SAVE_STATE__CURRENT_EXPRESSION, ""
                )
            )
            binding.formulaField.setText(localizedExpression)
        }

        adapter = Adapter(View.OnClickListener {
            when (it.id) {
                R.id.equals__button -> viewModel.evaluate(
                    binding.formulaField.text!!.toString(),
                    true,
                    binding.remoteToggle.isChecked
                )
                R.id.delete__button -> delete()
                R.id.all_cancel__button -> clear()
                R.id.sine__button,
                R.id.tangent__button,
                R.id.cosine__button,
                R.id.ln__button,
                R.id.log__button ->
                    // Add left parenthesis after functions.
                    binding.formulaField.append((it as TextView).text.toString() + "(")
                else -> binding.formulaField.append((it as TextView).text)
            }
        })
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        with(binding.formulaField) {
            setEditableFactory(object : Editable.Factory() {
                override fun newEditable(source: CharSequence?): Editable {
                    return ExpressionStringBuilder(source, tokenizer)
                }
            })
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    viewModel.evaluate(s.toString(), remote = binding.remoteToggle.isChecked)
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }

        with(binding.pager) {
            setPageTransformer(false, CubeOutRotationTransformer())
            adapter = this@CalculatorActivity.adapter
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(
            SAVE_STATE__CURRENT_EXPRESSION,
            tokenizer.getNormalizedExpression(binding.formulaField.text.toString())
        )
    }

    private fun clear() {
        binding.formulaField.text!!.clear()
        binding.resultField.text!!.clear()
    }

    private fun delete() {
        val formulaText = binding.formulaField.editableText
        val formulaLength = formulaText.length
        if (formulaLength > 0) {
            formulaText.delete(formulaLength - 1, formulaLength)
        }
    }

    private class Adapter(private val cl: View.OnClickListener) : PagerAdapter() {
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            return when (position) {
                0 -> ViewBasicBinding
                    .inflate(LayoutInflater.from(container.context), container, false)
                    .apply {
                        clickListener = cl
                    }
                1 -> ViewAdvancedBinding
                    .inflate(LayoutInflater.from(container.context), container, false)
                    .apply {
                        clickListener = cl
                    }
                else -> throw IllegalArgumentException("position $position")
            }.also {
                container.addView(it.root)
            }
        }

        override fun destroyItem(container: ViewGroup, position: Int, page: Any) {
            container.removeView((page as ViewDataBinding).root)
        }

        override fun isViewFromObject(view: View, page: Any) = view == (page as ViewDataBinding).root
        override fun getCount() = 2
    }

    private companion object {
        private val SAVE_STATE__CURRENT_EXPRESSION = this::class.java.name + "save_state__current_expression"
    }
}