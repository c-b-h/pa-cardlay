package se.ingenuity.cardlay.feature.calculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class CalculatorViewModel @Inject constructor(calculator: Calculator) : ViewModel() {
    private val expression = MutableLiveData<Pair<String, Boolean>>()

    val response = Transformations.switchMap(expression) {
        if (it.second) {
            calculator.remoteEval(it.first)
        } else {
            calculator.localEval(it.first)
        }
    }

    fun evaluate(expression: String, force: Boolean = false, remote: Boolean = false) {
        val newValue = expression to remote

        if (this.expression.value != newValue || force) {
            this.expression.value = newValue
        }
    }
}