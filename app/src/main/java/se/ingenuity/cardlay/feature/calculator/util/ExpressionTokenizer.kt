package se.ingenuity.cardlay.feature.calculator.util

import android.content.Context
import se.ingenuity.cardlay.R
import javax.inject.Inject

/**
 * Translation class between representational (localized) values and accepted values (normalized).
 */
class ExpressionTokenizer @Inject
internal constructor(context: Context) {
    private val replacementMap = mutableMapOf<String, String>()

    init {
        replacementMap["."] = context.getString(R.string.decimal_separator)

        replacementMap["0"] = context.getString(R.string.digit_0)
        replacementMap["1"] = context.getString(R.string.digit_1)
        replacementMap["2"] = context.getString(R.string.digit_2)
        replacementMap["3"] = context.getString(R.string.digit_3)
        replacementMap["4"] = context.getString(R.string.digit_4)
        replacementMap["5"] = context.getString(R.string.digit_5)
        replacementMap["6"] = context.getString(R.string.digit_6)
        replacementMap["7"] = context.getString(R.string.digit_7)
        replacementMap["8"] = context.getString(R.string.digit_8)
        replacementMap["9"] = context.getString(R.string.digit_9)

        replacementMap["/"] = context.getString(R.string.op_div)
        replacementMap["*"] = context.getString(R.string.op_mul)
        replacementMap["-"] = context.getString(R.string.op_sub)

        replacementMap["cos"] = context.getString(R.string.fun_cos)
        replacementMap["ln"] = context.getString(R.string.fun_ln)
        replacementMap["log"] = context.getString(R.string.fun_log)
        replacementMap["sin"] = context.getString(R.string.fun_sin)
        replacementMap["tan"] = context.getString(R.string.fun_tan)

        replacementMap["Infinity"] = context.getString(R.string.inf)
    }

    fun getNormalizedExpression(localizedExpression: String): String {
        var normalizedExpression = localizedExpression
        for ((key, value) in replacementMap) {
            normalizedExpression = normalizedExpression.replace(value, key)
        }
        return normalizedExpression
    }

    fun getLocalizedExpression(normalizedExpression: String): String {
        var localizedExpression = normalizedExpression
        for ((key, value) in replacementMap) {
            localizedExpression = localizedExpression.replace(key, value)
        }
        return localizedExpression
    }
}
