package se.ingenuity.cardlay.feature.calculator.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * There is no reason at this point to make this class [Parcelable] except satisfying the third requirement on the
 * nice-to-have list: 3. [Parcelable] model objects
 */
@Parcelize
data class CalculationResponse(val result: List<String>?, val error: String?) : Parcelable