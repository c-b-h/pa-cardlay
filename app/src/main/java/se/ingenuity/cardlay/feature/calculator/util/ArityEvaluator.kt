package se.ingenuity.cardlay.feature.calculator.util

import org.javia.arity.Symbols
import org.javia.arity.SyntaxException
import org.javia.arity.Util
import se.ingenuity.cardlay.R
import javax.inject.Inject

/**
 * This class uses an implementation of Java Arithmetics Engine to evaluate user's input.
 */
class ArityEvaluator @Inject
internal constructor(private val tokenizer: ExpressionTokenizer, private val normalizer: ExpressionNormalizer) {
    private val symbols: Symbols = Symbols()

    /**
     * Evaluates an expression in localized format such as 5+6,4×1.
     *
     * The above expression is first normalized to 5+6.4*1 before being passed to the engine.
     *
     * @param expr Localized arithmetic expression
     * @return a [Triple] containing
     * [Triple.first]: the normalized expression;
     * [Triple.second]: the calculation or null;
     * [Triple.third]: a string resource id or null entailing the possible error cause
     */
    fun evaluate(expr: CharSequence): Triple<String, String?, Int?> {
        val normalizedExpression = normalizer.normalize(expr.toString())

        if (normalizedExpression.isEmpty() || normalizedExpression.toDoubleOrNull() != null) {
            return Triple(normalizedExpression, null, null)
        }

        try {
            val result = symbols.eval(normalizedExpression)
            return if (result.isNaN()) {
                Triple(normalizedExpression, null, R.string.error_nan)
            } else {
                // The arity library uses floating point arithmetic when evaluating the expression
                // leading to precision errors in the result. The method doubleToString hides these
                // errors; rounding the result by dropping N digits of precision.
                val resultString = tokenizer.getLocalizedExpression(
                    Util.doubleToString(
                        result,
                        MAX_DIGITS,
                        ROUNDING_DIGITS
                    )
                )
                Triple(normalizedExpression, resultString, null)
            }
        } catch (e: SyntaxException) {
            return Triple(normalizedExpression, null, R.string.error_syntax)
        }
    }

    private companion object {
        /**
         * The maximum number of significant digits to display.
         */
        private const val MAX_DIGITS = 12

        /**
         * A [Double] has at least 17 significant digits, we show the first [.MAX_DIGITS]
         * and use the remaining digits as guard digits to hide floating point precision errors.
         */
        private val ROUNDING_DIGITS = Math.max(17 - MAX_DIGITS, 0)
    }
}
