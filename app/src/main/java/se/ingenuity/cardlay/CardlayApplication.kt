package se.ingenuity.cardlay

import dagger.android.support.DaggerApplication
import se.ingenuity.cardlay.di.AppComponent
import se.ingenuity.cardlay.di.DaggerAppComponent

class CardlayApplication : DaggerApplication() {
    private lateinit var appComponent : AppComponent

    override fun onCreate() {
        appComponent = DaggerAppComponent.builder().application(this).build()

        super.onCreate()
    }

    override fun applicationInjector() = appComponent
}