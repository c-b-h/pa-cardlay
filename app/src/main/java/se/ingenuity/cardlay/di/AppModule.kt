package se.ingenuity.cardlay.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module(includes = [FeaturesModule::class, LifecycleModule::class])
abstract class AppModule {
    @Binds
    abstract fun bindApplicationContext(application: Application): Context
}