package se.ingenuity.cardlay.di

import dagger.Module
import se.ingenuity.cardlay.feature.calculator.di.CalculatorModule

@Module(includes = [CalculatorModule::class])
interface FeaturesModule