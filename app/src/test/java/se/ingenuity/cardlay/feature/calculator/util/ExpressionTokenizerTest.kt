package se.ingenuity.cardlay.feature.calculator.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import se.ingenuity.cardlay.R

@RunWith(RobolectricTestRunner::class)
class ExpressionTokenizerTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    private lateinit var expressionTokenizer: ExpressionTokenizer

    @Before
    fun setUp() {
        expressionTokenizer = ExpressionTokenizer(context)
    }

    @Test
    fun expressionTokenizer_localizedExpression_normalizedExpression_add() {
        assertThat(
            expressionTokenizer.getNormalizedExpression(context.getString(R.string.op_add))
        ).isEqualTo("+")
    }

    @Test
    fun expressionTokenizer_normalizedExpression_localizedExpression_add() {
        assertThat(
            expressionTokenizer.getLocalizedExpression("+")
        ).isEqualTo(context.getString(R.string.op_add))
    }

    @Test
    fun expressionTokenizer_localizedExpression_normalizedExpression_sub() {
        assertThat(
            expressionTokenizer.getNormalizedExpression(context.getString(R.string.op_sub))
        ).isEqualTo("-")
    }

    @Test
    fun expressionTokenizer_normalizedExpression_localizedExpression_sub() {
        assertThat(
            expressionTokenizer.getLocalizedExpression("-")
        ).isEqualTo(context.getString(R.string.op_sub))
    }

    @Test
    fun expressionTokenizer_localizedExpression_normalizedExpression_mul() {
        assertThat(
            expressionTokenizer.getNormalizedExpression(context.getString(R.string.op_mul))
        ).isEqualTo("*")
    }

    @Test
    fun expressionTokenizer_normalizedExpression_localizedExpression_mul() {
        assertThat(
            expressionTokenizer.getLocalizedExpression("*")
        ).isEqualTo(context.getString(R.string.op_mul))
    }

    @Test
    fun expressionTokenizer_localizedExpression_normalizedExpression_div() {
        assertThat(
            expressionTokenizer.getNormalizedExpression(context.getString(R.string.op_div))
        ).isEqualTo("/")
    }

    @Test
    fun expressionTokenizer_normalizedExpression_localizedExpression_div() {
        assertThat(
            expressionTokenizer.getLocalizedExpression("/")
        ).isEqualTo(context.getString(R.string.op_div))
    }

    @Test
    fun expressionTokenizer_localizedExpression_normalized_composite() {
        // 65 - 10
        val compositeExpression = context.getString(R.string.digit_6) +
                context.getString(R.string.digit_5) +
                context.getString(R.string.op_sub) +
                context.getString(R.string.digit_1) +
                context.getString(R.string.digit_0)

        assertThat(
            expressionTokenizer.getNormalizedExpression(compositeExpression)
        ).isEqualTo("65-10")
    }
}