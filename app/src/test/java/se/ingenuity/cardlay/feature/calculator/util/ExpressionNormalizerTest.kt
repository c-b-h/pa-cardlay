package se.ingenuity.cardlay.feature.calculator.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ExpressionNormalizerTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    private lateinit var expressionNormalizer: ExpressionNormalizer

    @Before
    fun setUp() {
        expressionNormalizer = ExpressionNormalizer(ExpressionTokenizer(context))
    }

    @Test
    fun expressionNormalizer_trim_characters_before() {
        assertThat(
            expressionNormalizer.normalize("+/***-+65+4")
        ).isEqualTo("65+4")
    }


    @Test
    fun expressionNormalizer_trim_characters_after() {
        assertThat(
            expressionNormalizer.normalize("65+4+/***-+")
        ).isEqualTo("65+4")
    }
}