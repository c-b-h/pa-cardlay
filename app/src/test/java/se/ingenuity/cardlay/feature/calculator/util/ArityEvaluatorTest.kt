package se.ingenuity.cardlay.feature.calculator.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArityEvaluatorTest {
    private val context = ApplicationProvider.getApplicationContext<Context>()

    private lateinit var arityEvaluator: ArityEvaluator

    @Before
    fun setUp() {
        val tokenizer = ExpressionTokenizer(context)
        arityEvaluator = ArityEvaluator(tokenizer, ExpressionNormalizer(tokenizer))
    }

    @Test
    fun arityEvaluator_test_simple_addition() {
        assertThat(arityEvaluator.evaluate("90+10+1").second).isEqualTo("101")
    }

    @Test
    fun arityEvaluator_test_simple_subtraction() {
        assertThat(arityEvaluator.evaluate("101-90-10").second).isEqualTo("1")
    }

    @Test
    fun arityEvaluator_test_simple_multiplication() {
        assertThat(arityEvaluator.evaluate("128×2×2").second).isEqualTo("512")
    }

    @Test
    fun arityEvaluator_test_simple_multiplication_normalized() {
        assertThat(arityEvaluator.evaluate("128×2×2").first).isEqualTo("128*2*2")
    }

    @Test
    fun arityEvaluator_test_simple_division() {
        assertThat(arityEvaluator.evaluate("512÷2÷2").second).isEqualTo("128")
    }

    @Test
    fun arityEvaluator_test_addition_multiplication() {
        assertThat(arityEvaluator.evaluate("3+4×5").second).isEqualTo("23")
    }

    @Test
    fun arityEvaluator_test_subtraction_division() {
        assertThat(arityEvaluator.evaluate("3-4÷5").second).isEqualTo("2.2")
    }

    @Test
    fun arityEvaluator_test_divide_zero() {
        assertThat(arityEvaluator.evaluate("666÷0").second).isEqualTo("∞")
    }
}